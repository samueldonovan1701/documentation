---
title : "Source Control System"
chapter : false
weight : 2
pre: "<b>2. </b>"
---

Which source control system is/was used? How was it setup? How does a developer connect to it?