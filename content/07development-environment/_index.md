---
title : "Deployment Environment"
chapter : true
weight : 7
pre: "<b>7. </b>"
---


### Section 7

# Deployment Environment

The basic purpose for this section is to give a developer all of the necessary information to setup their development environment to build, run, test, and/or develop. One team member should write this section and have other team members best this section of the document