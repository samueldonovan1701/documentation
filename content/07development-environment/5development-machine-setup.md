---
title : "Development Machine Setup"
chapter : false
weight : 5
pre: "<b>5. </b>"
---

If warranted, provide a list of steps and details associated with setting up a machine for use by a developer.