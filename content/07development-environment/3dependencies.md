---
title : "Dependencies"
chapter : false
weight : 3
pre: "<b>3. </b>"
---

Describe all dependencies associated with developing the system.