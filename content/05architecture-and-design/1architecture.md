---
title : "Architecture"
chapter : false
weight : 1
pre: "<b>1. </b>"
---

Best represented by discussion and diagram.

See https://learn.netlify.app/en/shortcodes/mermaid/

See https://mermaid-js.github.io/mermaid/#/

{{<mermaid align="left">}}
graph LR;
	A[Component #1] --> B
	B[(Component #2)] --> C
	C(Component #3)

{{</mermaid>}}
