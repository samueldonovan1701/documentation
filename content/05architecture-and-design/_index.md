---
title : "Architecture and Design"
chapter : true
weight : 5
pre: "<b>5. </b>"
---

### Section 5

# Architecture and Design

This section is used to describe the design details for each of the major components in the system. This section is not brief and requires the necessary detail that can be used by the reader to truly understand the architecture and design details without having to dig into the code.

The structure of this section is really dependent on the system being represented by this document. Here is a list of artifacts or sections to consider.
