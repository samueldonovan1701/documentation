---
title : "Designs"
chapter : false
weight : 5
pre: "<b>5. </b>"
---

This is where the design details are presented and may contain subsections.

- Wirefames
- User or Data Flows
- Business Process Flows
- Algorithms
- Psuedocode