---
title : "Data Flow Diagram"
chapter : false
weight : 2
pre: "<b>2. </b>"
---


Is this needed or a better way to describe the system via a diagram?

See https://learn.netlify.app/en/shortcodes/mermaid/

See https://mermaid-js.github.io/mermaid/#/

{{<mermaid align="left">}}
graph LR;
	A[Component #1] --> B
	B[(Component #2)] --> C
	C(Component #3)

{{</mermaid>}}