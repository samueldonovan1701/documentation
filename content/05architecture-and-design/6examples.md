---
title : "Examples"
chapter : false
katex : true
weight : 6
pre: "<b>6. </b>"
---

These are example snippets that can be incorporated in the sections prior where applicable.

## Math

KaTeX can be used to generate complex math formulas. It supports in-line math using the `\\( ... \\)`delimiters, like this: \\(E = mc^2\\). By default, it does not support in-line delimiters `$...$` because those occur too commonly in typical webpages. It supports displayed math using the `$$` or `\\[...\\]` delimiters, like this:

### Example 1

If the text between `$$` contains newlines it will rendered in display mode:

```
$$
f(x) = \int_{-\infty}^\infty\hat f(\xi)\,e^{2 \pi i \xi x}\,d\xi
$$
```

$$
f(x) = \int_{-\infty}^\infty\hat f(\xi)\,e^{2 \pi i \xi x}\,d\xi
$$

### Example 2

```
$$
\frac{1}{\Bigl(\sqrt{\phi \sqrt{5}}-\phi\Bigr) e^{\frac25 \pi}} = 1+\frac{e^{-2\pi}} {1+\frac{e^{-4\pi}} {1+\frac{e^{-6\pi}} {1+\frac{e^{-8\pi}} {1+\cdots} } } }
$$
```

$$
\frac{1}{\Bigl(\sqrt{\phi \sqrt{5}}-\phi\Bigr) e^{\frac25 \pi}} = 1+\frac{e^{-2\pi}} {1+\frac{e^{-4\pi}} {1+\frac{e^{-6\pi}} {1+\frac{e^{-8\pi}} {1+\cdots} } } }
$$


### Example 3
```
$$
1 +  \frac{q^2}{(1-q)}+\frac{q^6}{(1-q)(1-q^2)}+\cdots = \prod_{j=0}^{\infty}\frac{1}{(1-q^{5j+2})(1-q^{5j+3})}, \quad\quad \text{for }\lvert q\rvert<1.
$$
```
$$
1 +  \frac{q^2}{(1-q)}+\frac{q^6}{(1-q)(1-q^2)}+\cdots = \prod_{j=0}^{\infty}\frac{1}{(1-q^{5j+2})(1-q^{5j+3})}, \quad\quad \text{for }\lvert q\rvert<1.
$$

### Example 4

Remember, certain characters are rendered by markdown, so you may need to workaround those issues. You can find the complete list of KaTeX supported functions here: https://khan.github.io/KaTeX/docs/supported.html

For example, the `'` character can be replaced with `^\prime`:

$$
G^\prime = G - u
$$

The `"` character can be replaced with `{\prime\prime}`:

$$
G^{\prime\prime} = G^\prime - v
$$


## Citations

The `ref` and `relref` shortcodes generate the absolute and relative permalinks to a document, respectively.

```
[Project Scope]({{< ref "01overview/1project-scope.md" >}})
```
Produces

[Project Scope]({{< ref "/01overview/1project-scope.md" >}})


```
[Data Flow Diagram]({{< relref "2data-flow-diagram.md" >}})
```
Produces

[Data Flow Diagram]({{< relref "2data-flow-diagram.md" >}})


## Code

The following is a code sample using syntax highlighting.

The following is a code sample using triple backticks ( \`\`\`) code fencing provided in Hugo.

```
```javascipt
var num1, num2, sum
num1 = prompt("Enter first number")
num2 = prompt("Enter second number")
sum = parseInt(num1) + parseInt(num2) // "+" means "add"
alert("Sum = " + sum)  // "+" means combine into a string
​```
```

turns into

```javascript
var num1, num2, sum
num1 = prompt("Enter first number")
num2 = prompt("Enter second number")
sum = parseInt(num1) + parseInt(num2) // "+" means "add"
alert("Sum = " + sum)  // "+" means combine into a string
```
    