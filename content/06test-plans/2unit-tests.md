---
title : "Unit Tests"
chapter : false
weight : 2
pre: "<b>2. </b>"
---

Lowest level of testing. Perform White Box testing method and typically automated. Functional testing.

## Dependencies

Describe the basic dependencies which should include setup, seed data, unit testing frameworks, and reference
material.

## Tools

Are there any?

## Documentation

Is there a resulting test plan execution results or report that can be referenced or linked here?