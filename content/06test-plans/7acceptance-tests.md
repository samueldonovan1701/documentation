---
title : "Acceptance Tests"
chapter : false
weight : 7
pre: "<b>7. </b>"
---

Evaluate the system’s compliance with the business requirements and assess whether it is acceptable for delivery. How will the stakeholders test and provide feedback and acceptance of the system or product?

## Dependencies

Describe the basic dependencies which should include setup, seed data, unit testing frameworks, and reference
material.

## Tools

Are there any?

## Documentation

Is there a resulting test plan execution results or report that can be referenced or linked here?