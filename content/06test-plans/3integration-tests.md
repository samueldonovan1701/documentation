---
title : "Integration Tests"
chapter : false
weight : 3
pre: "<b>3. </b>"
---

Next level of testing. Test integration between components of the same system and any interfaces to external systems. Functional testing.

## Dependencies

Describe the basic dependencies which should include setup, seed data, unit testing frameworks, and reference
material.

## Tools

Are there any?

## Documentation

Is there a resulting test plan execution results or report that can be referenced or linked here?