---
title : "Objectives"
chapter : false
weight : 1
pre: "<b>1. </b>"
---

Provides a brief overview of the testing approach, testing frameworks, and general how testing is/will be done to provide a measure of quality for the system. 

Are you planning to test every aspect of the system? How far with you take the testing effort, ex. manual, semi-automated, fully-automated?

You should really try to describe each level of test plan below with sections. 