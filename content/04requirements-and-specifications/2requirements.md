---
title : "Requirements"
chapter : false
weight : 2
pre: "<b>2. </b>"
---

## Design Constraints

Describe

- Windows Server?
- AWS or Azure?
- If mobile, can operate offline or disconnected?
- Local file or database storage limits and capabilities?

## System Requirements

Describe. How will they impact the potential design? Are there alternatives?

## Network Requirements

Describe. Is latency a factor?

## Development Environment Requirements

What are they? Is the system supposed to be cross-platform?

## Other Requirements

Describe
