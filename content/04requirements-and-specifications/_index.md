---
title : "Requirements and Specifications"
chapter : true
weight : 4
pre: "<b>4. </b>"
---

### Section 4

# Requirements and Specifications

Describe any specific functional (ex. need to use FastAPI) or non-functional (ex. cloud services cost threshold or specific performance metrics) requirements provided by the stakeholders or found to be needed for the system.

If there are detailed textitspecifications (ex. signal-noise ratio held to a certain tolerance), those should be documented in this section.

This section can describe any number of requirements or specifications, so add sections and subsections where appropriate.
