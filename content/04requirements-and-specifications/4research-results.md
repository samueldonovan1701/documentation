---
title : "Research Results"
chapter : false
weight : 4
pre: "<b>4. </b>"
---

This section is reserved for the discussion centered on any research that needed to take place before full system design. The research efforts may have led to the need to actually provide a proof of concept for approval by the stakeholders.

The proof of concept might even go to the extent of a user interface design or mockups