---
title : "Milestone Overview"
chapter : false
weight : 4
pre: "<b>4. </b>"
---

Describe how the project is/was broken up into phases of deliverables. Are they phases, milestones, or iterations that you worked out with your sponsor.
