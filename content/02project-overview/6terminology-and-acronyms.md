---
title : "Terminology and Acronyms"
chapter : false
weight : 6
pre: "<b>6. </b>"
---

Provide a list of terms used in the document that warrant definition. Consider industry or domain specific terms and acronyms as well as system specific.
