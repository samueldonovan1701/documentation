---
title : "Process"
chapter : false
weight : 2
pre: "<b>2. </b>"
---

Describe the process used to execute the project.

- Is there a process framework (Waterfall, Agile, Scrum, etc.) being used?
- What is the incremental delivery or deliverable strategy?
- What project management tools are being used?
- What is the communication protocol?
- Where is the code and documentation stored?
- What project management tools are being used?
- Are there restrictions that should be noted?
