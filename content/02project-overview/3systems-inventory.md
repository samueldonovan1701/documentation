---
title : "Systems Inventory"
chapter : false
weight : 3
pre: "<b>3. </b>"
---

Provide a description and link to the location of the resources. For example, GitLab, Google Drive, Teams, or description and links to the industry sponsored projects systems (regardless whether the reader has access). In general, what did you use and have access to as an artifact of the project execution.
