---
title : "Team Members and Roles"
chapter : false
weight : 1
pre: "<b>1. </b>"
---

Describe who was involved and what role(s) were played. This should include sponsors, stakeholders, and advisors. Is there a team lead? Was there a breakdown between front-end and back-end or embedded system and control system?
