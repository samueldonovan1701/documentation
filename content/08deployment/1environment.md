---
title : "Environment"
chapter : false
weight : 1
pre: "<b>1. </b>"
---

Describe server, operating systems, or databases that makeup the production environment.