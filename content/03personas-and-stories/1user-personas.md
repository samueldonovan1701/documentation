---
title : "User Personas"
chapter : false
weight : 1
pre: "<b>1. </b>"
---

Describe the users or roles associated with the system. What jobs do they need to perform? How do they perform the those jobs? 

This helps the reader better understand the user stories.