---
title : "User Stories"
chapter : false
weight : 2
pre: "<b>2. </b>"
---

This section should be the result of discussions with the stakeholders regarding the actual functional needs or jobs to be done by the system. It is up to the team how much detail is updated in this section, but it should have a fairly complete list of user stories. The team might not need to breakdown all of the user stories in this document, but a few stories might warrant documenting the breakdown into smaller, related stories.

This section should give the reader a picture of what the system is/was intended to accomplish.

Functional requirements and specifications of the system can be documented in the next section.


###  As a {role}, I {want/need} to {do something} so that {reason}

Any discussion?


###  As a {role}, I {want/need} to {do something} so that {reason}

Any discussion?


###  As a {role}, I {want/need} to {do something} so that {reason}

Any discussion?