---
title : "Home"
chapter: true
---


# Product or System Title
## Senior Design System Documentation

Provide a summary of the document contents, the intended reader and audience, etc.

### Group Members

| Name           | Role              | Contact |
| -------------- | ----------------- | ------- |
| Group Member A | Frontend          | <ul><li>emailA@gmail.com</li><li>(605)000-0000</li></ul> |
| Group Member B | Backend           | emailB@gmail.com |
| Group Member C | DevOps & Buisness | emailC@gmail.com <br /> [LinkedIn](https://linkedin.com) | 