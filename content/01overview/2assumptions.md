---
title : "Assumptions"
chapter : false
weight : 2
pre: "<b>2. </b>"
---

Conditions and resources that need to be present and accessible for the system to work as described.