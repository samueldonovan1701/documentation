---
title : "Architecture"
chapter : false
weight : 5
pre: "<b>5. </b>"
description: "Testing"
---

Provide a high-level view and description about the architecture of the system.

## Diagram

See https://learn.netlify.app/en/shortcodes/mermaid/

See https://mermaid-js.github.io/mermaid/#/

{{<mermaid align="left">}}
graph LR;
	A[Component #1] --> B
	B[(Component #2)] --> C
	C(Component #3)

{{</mermaid>}}

---

## Major Component

Describe briefly the role this major component plays in this system.

---

## Major Component

Describe briefly the role this major component plays in this system.

---

## Major Component

Describe briefly the role this major component plays in this system.