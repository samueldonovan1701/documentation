---
chapter : false
title : "Project Scope"
weight : 1
pre: "<b>1. </b>"
---

- What scope does this document cover?
- What is the context or the time frame?
- What is out of scope?