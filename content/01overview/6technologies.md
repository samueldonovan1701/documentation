---
title : "Technologies"
chapter : false
weight : 6
pre: "<b>6. </b>"
---

This section should contain a list of specific technologies used to develop the system. The list should contain the name of the technology, brief description, link to reference material for further understanding, and briefly how/where/why it was used in the system.