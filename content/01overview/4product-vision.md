---
title : "Product Vision"
chapter : false
weight : 4
pre: "<b>4. </b>"
---

Typically provided by the stakeholders and can take the following form. The product vision or position statement is what guides the project team in making decisions about the product. If the stakeholder did not provide one, build your own based on your understanding and it does not have to take this form.

- For [target customer]
- Who [statement of need or opportunity]
- The [product name] is a [product category]
- That [key benefit or reason to buy]
- Unlike [primary competitive alternative]
- Our Product [statement of differentiation]