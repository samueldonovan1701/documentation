---
title : "Purpose"
chapter : false
weight : 3
pre: "<b>3. </b>"
---

- What is the purpose?
- What problem is being solved?