---
title : "User Guide"
chapter : false
weight : 1
pre: "<b>1. </b>"
---

The source for the user guide can go here. You have some options for how to handle the user docs. If you
have some newpage commands around the guide then you can just print out those pages.
If a different formatting is required, then have the source in a separate file userguide.tex and include that
file here. That file can also be included into a driver (like the system template) which has the client specified
formatting. Again, this is a single source approach.