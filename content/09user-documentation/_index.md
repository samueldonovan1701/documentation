---
title : "User Documentation"
chapter : true
weight : 9
pre: "<b>9. </b>"
---


### Section 9

# User Documentation

This section should contain the basis for any end-user documentation for the system. End-user documentation would cover the basic steps for setup and use of the system. It is likely that the majority of this section would be present in its own document to be delivered to the end-user. However, it is recommended the original is contained and maintained in this document.
