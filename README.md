# Senior Design Documentation

[[_TOC_]]

---

This project uses [GitLab Pages][gitlab_pages] and [Hugo][hugo] (using the [Learn Theme][hugo_learn]) to serve sample documentation for SDSM&T CSC & CENG Senior Design.

The generated pages are hosted at https://samueldonovan1701.gitlab.io/documentation/

# Forking this project

If you decide to fork this project, a few things will have to be changed for it to function properly on GitLab Pages.

At a minimum, you'll need to configure your site: the `baseUrl` line in your `config.yaml`, from `https://samueldonovan1701.gitlab.io/documentation/` to `https://{user/groupname}.gitlab.io/{projectname}`.

You should also change any other pertinant information in the config file to correspond to your project, noteably `editURL` and the `GitLab Repo` menu shortcut item url.


# Local Development

You do not need to install Hugo to develop this project locally: only if you want to preview the site before pushing changes is it neccesary.

## Installing Hugo

Official installation instructions can be found [here](https://gohugo.io/getting-started/installing/). Below is a brief outline.

This project was designed for [Hugo v0.88.1](https://github.com/gohugoio/hugo/releases/tag/v0.88.1)

### Docker

Hugo currently does not offer official Hugo images for Docker, but they do recommend these up to date distributions: https://hub.docker.com/r/klakegg/hugo/

### Package Manager

Hugo exists on many package managers, including but not limited to Apt (`apt`), Homebrew (`brew`), MacPorts (`port`), Chocolatey (`choco`) and Scoop (`scoop)`.

To install through a package manager, e.g. Apt, run

```sh
apt install hugo
```

### Compiled Binary

[Hugo v0.88.1](https://github.com/gohugoio/hugo/releases/tag/v0.88.1) has pre-built binaries for the following:

- macOS (Darwin) : [x64](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_macOS-64bit.tar.gz), and [ARM](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_macOS-ARM64.tar.gz)
- Windows :        [32-bit](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_Windows-32bit.zip), [64-bit](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_Windows-64bit.zip), and [ARM](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_Windows-ARM.zip)
- Linux :          [32-bit](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_Linux-32bit.tar.gz), [64-bit](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_Linux-64bit.tar.gz), and [ARM](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_Linux-ARM.tar.gz), and [ARM64](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_Linux-ARM64.tar.gz) 
- OpenBSD :        [32-bit](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_OpenBSD-32bit.tar.gz), [64-bit](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_OpenBSD-64bit.tar.gz), and [ARM](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_OpenBSD-ARM.tar.gz), and [ARM64](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_OpenBSD-ARM64.tar.gz) 
- FreeBSD :        [32-bit](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_FreeBSD-32bit.tar.gz), [64-bit](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_FreeBSD-64bit.tar.gz), and [ARM](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_FreeBSD-ARM.tar.gz), and [ARM64](https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_0.88.1_FreeBSD-ARM64.tar.gz) 


The compiled binary can either be put into the project directory (which may slightly change how you run the project), or ideally be put in your `PATH` (most likely `/usr/local/bin`, or `C:\Windows\system32` on Windows)

## Generating & Running the Site

In the project directory, run the following in your console:

```
hugo server
```

This will generate the website and start a locally hosted server, available at [http://localhost:1313/documentation/](http://localhost:1313/documentation/). This server has LiveRefresh built in, so you can simply refresh the webpage to view your changes.

Running just `hugo` will generate the site's files without launching a server. Run `hugo help` for more, or check out the [Hugo Documentation][hugo_docs].

# Directory Structure

## Content

The `/content` folder is where the files that will be translated into the body of the webpage live. 

The Hugo Learn Theme defines two types of pages: __Chapters__ and __Default__. 

__Chapters__ are typically title-card like, and contain `chapter: true` in their front matter (the first few lines of the file). __Default__ pages do not contain the key in their frontmatter, and should be where most content lives. 

Read more on the [Hugo Documentation](https://gohugo.io/content-management/) or about [Learn Theme Pages](https://learn.netlify.app/en/cont/pages/).

## Configuration

Hugo ships with a large number of configuration directives. The `config.yaml` file is where those directives exist.

See the [Hugo config directives](https://gohugo.io/getting-started/configuration/#all-variables-yaml) and [Learn Theme directives](https://learn.netlify.app/en/basics/configuration/) for more information.

## Layouts

`/layouts` stores templates in the form of `.html` files that define parts of your web pages.

Templates include [list pages](https://gohugo.io/templates/list/), your [homepage](https://gohugo.io/templates/homepage/), [taxonomy templates](https://gohugo.io/templates/taxonomy-templates/), [partials](https://gohugo.io/templates/partials/), [single page templates](https://gohugo.io/templates/single-page-templates/), and more.

In particular instrest for this project is the `/layouts/partials/logo.html` file, which is part of the Learn Theme and descibes the logo that appears in the top left of the webpage. `/layouts/partials/custom-header.html` is where content that needs to go in `<head>` should go, such as CSS file or javascript file inclusions.

## Static

`/static` hold all the file to be copied as-is to be served. This commonly incldues CSS, JavaScript, and image files.

Here, `/static/favicon.png` defines the favicon of this website.

## Themes

Hugo themes can be installed by copying them to `/themes` and editing `theme :` in `config.yaml`. Be default, this project uses [Learn Theme for Hugo][hugo_learn]


# Explanation

This project is possible through GitLab Pages, GitLab CI/CD, and Hugo. Read below for short explanations on how.

## GitLab CI/CD

[GitLab CI/CD Pipelines][gitlab_cicd] are basically scripts that get ran when a commit is made. This means you can compile your committed code and automatically generate a new release. 

This project's pipeline only runs when the default branch is committed to (in this case `master`). It generates the site's files using `hugo`, and copies the resulting items (artifacts) to the `public` folder.

For more, read the [GitLab CI/CD Documentation][gitlab_cicd].

## GitLab Pages

[GitLab Pages][gitlab_pages] allows you to serve static files from your GitLab repository via the web (usually HTML, CSS, and JS files).

For GitLab Pages to work, you must have a job in your CI Pipeline named `pages`. Files to be served must appear in your repository under the `public` folder by the end of this job.

For more, read the [GitLab Pages Documentation][gitlab_pages].

## Hugo

[Hugo][hugo] is a Static Site Generator (SSG), which means that we can write minimalistic files and have Hugo generate all the neccesary code needed for a nice looking webpage. How the end product looks is determined by our installed theme, [Learn][hugo_learn].

In this project, we use our CI/CD pipeline to generate the static website files which then get copied into `public` so they can be served by GitLab pages.

For more, read the [Hugo Documentation][hugo_docs] and [Hugo Learn Theme Documentation][hugo_learn].

---

# Resources

- [GitLab Pages Documentation][gitlab_pages]
- [GitLab CI/CD][gitlab_cicd]
- [Hugo Documentation][hugo_docs]
- [Example Hugo website using GitLab Pages][hugo_example]
- [Hugo Learn Theme Documentation][hugo_learn]

[generated_pages]: https://samueldonovan1701.gitlab.io/documentation/
[hugo]: https://gohugo.io/
[hugo_docs]: https://gohugo.io/documentation/
[hugo_learn]: https://learn.netlify.app/en/
[hugo_example]: https://gitlab.com/pages/hugo
[git_dl]: https://git-scm.com/downloads
[gitlab_cicd]: https://docs.gitlab.com/ee/ci/
[gitlab_pages]: https://docs.gitlab.com/ee/user/project/pages/
